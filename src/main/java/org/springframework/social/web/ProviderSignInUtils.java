package org.springframework.social.web;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.web.context.request.RequestAttributes;

public class ProviderSignInUtils {

  private OAuth2SessionStrategy sessionStrategy;
  private ConnectionFactoryLocator connectionFactoryLocator;
  private UsersConnectionRepository connectionRepository;

  public ProviderSignInUtils(OAuth2SessionStrategy sessionStrategy,
                             ConnectionFactoryLocator connectionFactoryLocator,
                             UsersConnectionRepository connectionRepository) {
    this.sessionStrategy = sessionStrategy;
    this.connectionFactoryLocator = connectionFactoryLocator;
    this.connectionRepository = connectionRepository;
  }

  public Connection<?> getConnectionFromSession(RequestAttributes request) {
    ProviderSignInAttempt signInAttempt = sessionStrategy.getSignInAttempt(request);
    return signInAttempt != null ? signInAttempt.getConnection(connectionFactoryLocator) : null;
  }

  public void doPostSignUp(String userId, RequestAttributes request) {
    ProviderSignInAttempt signInAttempt = sessionStrategy.getSignInAttempt(request);
    if (signInAttempt == null) {
      return;
    }
    signInAttempt.addConnection(userId, connectionFactoryLocator, connectionRepository);
    sessionStrategy.destroySession(request);
  }
}