package org.springframework.social.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.support.OAuth1ConnectionFactory;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static java.util.Arrays.asList;
import static org.springframework.social.web.OAuth2RequestParameters.PARAMETER_STATE;
import static org.springframework.social.web.OAuth2RequestParameters.SCOPE;

public class OAuth2ConnectSupport {

  private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2ConnectSupport.class);

  private boolean useAuthenticateUrl;
  private String applicationUrl;
  private String callbackUrl;
  private OAuth2SessionStrategy sessionStrategy;

  public OAuth2ConnectSupport(OAuth2SessionStrategy sessionStrategy) {
    this.sessionStrategy = sessionStrategy;
  }

  public void setUseAuthenticateUrl(boolean useAuthenticateUrl) {
    this.useAuthenticateUrl = useAuthenticateUrl;
  }

  public void setApplicationUrl(String applicationUrl) {
    this.applicationUrl = applicationUrl;
  }

  public void setCallbackUrl(String callbackUrl) {
    this.callbackUrl = callbackUrl;
  }

  public String buildOAuthUrl(ConnectionFactory<?> connectionFactory,
                              NativeWebRequest request,
                              MultiValueMap<String, String> additionalParameters) {
    if (connectionFactory instanceof OAuth1ConnectionFactory) {
      throw new IllegalArgumentException("OAuth1 ConnectionFactory not supported yet!");
    } else if (connectionFactory instanceof OAuth2ConnectionFactory) {
      return buildOAuth2Url((OAuth2ConnectionFactory<?>) connectionFactory, request, additionalParameters);
    } else {
      throw new IllegalArgumentException("ConnectionFactory not supported");
    }
  }

  public <A> Connection<A> completeConnection(OAuth2ConnectionFactory<A> connectionFactory, NativeWebRequest request) {
    if (connectionFactory.supportsStateParameter()) {
      verifyStateParameter(request);
    }
    try {
      String code = request.getParameter("code");
      AccessGrant accessGrant = connectionFactory
          .getOAuthOperations()
          .exchangeForAccess(code, callbackUrl(request), null);
      return connectionFactory.createConnection(accessGrant);
    } catch (HttpClientErrorException e) {
      LOGGER.error("HttpClientErrorException while completing connection: ", e);
      LOGGER.warn("Response body: {}", e.getResponseBodyAsString());
      throw e;
    }
  }

  private void verifyStateParameter(NativeWebRequest request) {
    String state = request.getParameter(PARAMETER_STATE);
    String originalState = extractCachedOAuth2State(request);
    if (state == null || !state.equals(originalState)) {
      throw new IllegalStateException("The OAuth2 'state' parameter is missing or doesn't match.");
    }
  }

  protected String callbackUrl(NativeWebRequest request) {
    if (callbackUrl != null) {
      return callbackUrl;
    }
    HttpServletRequest nativeRequest = request.getNativeRequest(HttpServletRequest.class);
    if (applicationUrl != null) {
      return applicationUrl + connectPath(nativeRequest);
    } else {
      return nativeRequest.getRequestURL().toString();
    }
  }

  private String buildOAuth2Url(OAuth2ConnectionFactory<?> connectionFactory, NativeWebRequest request, MultiValueMap<String, String> additionalParameters) {
    OAuth2Operations oauthOperations = connectionFactory.getOAuthOperations();
    String defaultScope = connectionFactory.getScope();
    OAuth2Parameters parameters = getOAuth2Parameters(request, defaultScope, additionalParameters);
    String state = connectionFactory.generateState();
    parameters.add(PARAMETER_STATE, state);
    sessionStrategy.createSessionWithState(request, state);
    if (useAuthenticateUrl) {
      return oauthOperations.buildAuthenticateUrl(parameters);
    } else {
      return oauthOperations.buildAuthorizeUrl(parameters);
    }
  }

  private OAuth2Parameters getOAuth2Parameters(NativeWebRequest request, String defaultScope, MultiValueMap<String, String> additionalParameters) {
    OAuth2Parameters parameters = new OAuth2Parameters(additionalParameters);
    parameters.putAll(getRequestParameters(request, SCOPE));
    parameters.setRedirectUri(callbackUrl(request));
    String scope = request.getParameter(SCOPE);
    if (scope != null) {
      parameters.setScope(scope);
    } else if (defaultScope != null) {
      parameters.setScope(defaultScope);
    }
    return parameters;
  }

  private String connectPath(HttpServletRequest request) {
    String pathInfo = request.getPathInfo();
    return request.getServletPath() + (pathInfo != null ? pathInfo : "");
  }

  private String extractCachedOAuth2State(WebRequest request) {
    return sessionStrategy.getState(request);
  }

  private MultiValueMap<String, String> getRequestParameters(NativeWebRequest request, String... ignoredParameters) {
    List<String> ignoredParameterList = asList(ignoredParameters);
    MultiValueMap<String, String> convertedMap = new LinkedMultiValueMap<>();
    request.getParameterMap().forEach((key, value) -> {
      if (!ignoredParameterList.contains(key)) {
        convertedMap.put(key, asList(value));
      }
    });
    return convertedMap;
  }
}

