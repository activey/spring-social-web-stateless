package org.springframework.social.web;

import org.springframework.social.connect.Connection;
import org.springframework.web.context.request.NativeWebRequest;

public interface SignInAdapter<A> {

  String signIn(String userId, Connection<A> connection, NativeWebRequest request);
}