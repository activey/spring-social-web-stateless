package org.springframework.social.web.session;

import org.springframework.social.web.OAuth2SessionStrategy;
import org.springframework.social.web.ProviderSignInAttempt;
import org.springframework.web.context.request.RequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.social.web.OAuth2RequestParameters.PARAMETER_STATE;

public abstract class AbstractIdentifiedSessionStrategy implements OAuth2SessionStrategy {

  @Override
  public void createSessionWithState(RequestAttributes request, String state) {
    Session session = newSession(state);
    session.setState(state);
  }

  @Override
  public void setSignInAttempt(RequestAttributes request, ProviderSignInAttempt signInAttempt) {
    Session session = acquireSession(request);
    session.setSignInAttempt(signInAttempt);
    updateSession(session);
  }

  @Override
  public ProviderSignInAttempt getSignInAttempt(RequestAttributes request) {
    return acquireSession(request).getSignInAttempt();
  }

  @Override
  public String getState(RequestAttributes request) {
    return acquireSession(request).getState();
  }

  @Override
  public void destroySession(RequestAttributes request) {
    removeSession(acquireSession(request).getState());
  }

  private Session acquireSession(RequestAttributes request) {
    // nasty casting ;(
    HttpServletRequest httpServletRequest = (HttpServletRequest) request.resolveReference(RequestAttributes.REFERENCE_REQUEST);
    String sessionStateId = httpServletRequest.getParameter(PARAMETER_STATE);
    return findSession(sessionStateId);
  }

  protected abstract void updateSession(Session session);

  protected abstract Session newSession(String sessionStateId);

  protected abstract Session findSession(String sessionStateId);

  protected abstract void removeSession(String sessionStateId);
}
