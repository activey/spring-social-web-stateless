package org.springframework.social.web.session;

import org.springframework.social.web.ProviderSignInAttempt;

import java.io.Serializable;
import java.util.Objects;

public class Session implements Serializable {

  private ProviderSignInAttempt signInAttempt;
  private String state;

  public void setSignInAttempt(ProviderSignInAttempt signInAttempt) {
    this.signInAttempt = signInAttempt;
  }

  public ProviderSignInAttempt getSignInAttempt() {
    return signInAttempt;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getState() {
    return state;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Session session = (Session) o;
    return Objects.equals(signInAttempt, session.signInAttempt) &&
        Objects.equals(state, session.state);
  }

  @Override
  public int hashCode() {
    return Objects.hash(signInAttempt, state);
  }
}
