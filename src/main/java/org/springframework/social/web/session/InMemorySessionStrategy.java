package org.springframework.social.web.session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemorySessionStrategy extends AbstractIdentifiedSessionStrategy {

  private Map<String, Session> sessionMap = new ConcurrentHashMap<>();

  @Override
  protected Session newSession(String sessionStateId) {
    Session session = new Session();
    session.setState(sessionStateId);
    sessionMap.put(sessionStateId, session);
    return session;
  }

  @Override
  protected void updateSession(Session session) {
    // not necessary
  }

  @Override
  protected Session findSession(String sessionStateId) {
    return sessionMap.get(sessionStateId);
  }

  @Override
  protected void removeSession(String sessionStateId) {
    sessionMap.remove(sessionStateId);
  }
}
