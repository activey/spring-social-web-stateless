package org.springframework.social.web;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;

import java.io.Serializable;
import java.util.Objects;

public class ProviderSignInAttempt implements Serializable {

  private ConnectionData connectionData;

  public ProviderSignInAttempt(ConnectionData connectionData) {
    this.connectionData = connectionData;
  }

  public ProviderSignInAttempt() {
  }

  public Connection<?> getConnection(ConnectionFactoryLocator connectionFactoryLocator) {
    return connectionFactoryLocator
        .getConnectionFactory(connectionData.getProviderId())
        .createConnection(connectionData);
  }

  void addConnection(String userId, ConnectionFactoryLocator connectionFactoryLocator, UsersConnectionRepository connectionRepository) {
    connectionRepository
        .createConnectionRepository(userId)
        .addConnection(getConnection(connectionFactoryLocator));
  }

  public ConnectionData getConnectionData() {
    return connectionData;
  }

  public void setConnectionData(ConnectionData connectionData) {
    this.connectionData = connectionData;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProviderSignInAttempt that = (ProviderSignInAttempt) o;
    return Objects.equals(connectionData, that.connectionData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(connectionData);
  }
}
