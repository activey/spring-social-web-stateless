package org.springframework.social.web;

import org.springframework.web.context.request.RequestAttributes;

public interface OAuth2SessionStrategy {

  void setSignInAttempt(RequestAttributes request, ProviderSignInAttempt signInAttempt);

  ProviderSignInAttempt getSignInAttempt(RequestAttributes request);

  void createSessionWithState(RequestAttributes request, String state);

  String getState(RequestAttributes request);

  void destroySession(RequestAttributes request);
}
