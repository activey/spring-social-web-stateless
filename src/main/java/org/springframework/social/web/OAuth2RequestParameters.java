package org.springframework.social.web;

public class OAuth2RequestParameters {

  public static final String PARAMETER_STATE = "state";
  public static final String SCOPE = "scope";
  public static final String PARAMETER_CODE = "code";
  public static final String PARAMETER_ERROR = "error";
  public static final String PARAMETER_ERROR_DESCRIPTION = "error_description";
  public static final String PARAMETER_ERROR_URI = "error_uri";

  public static final String ERROR_CODE_PROVIDER = "provider";
  public static final String ERROR_CODE_MULTIPLE_USERS = "multiple_users";
}
