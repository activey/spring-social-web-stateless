package org.springframework.social.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.support.URIBuilder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.util.Optional.ofNullable;
import static org.springframework.social.web.OAuth2RequestParameters.ERROR_CODE_MULTIPLE_USERS;
import static org.springframework.social.web.OAuth2RequestParameters.ERROR_CODE_PROVIDER;
import static org.springframework.social.web.OAuth2RequestParameters.PARAMETER_CODE;
import static org.springframework.social.web.OAuth2RequestParameters.PARAMETER_ERROR;
import static org.springframework.social.web.OAuth2RequestParameters.PARAMETER_ERROR_DESCRIPTION;
import static org.springframework.social.web.OAuth2RequestParameters.PARAMETER_ERROR_URI;
import static org.springframework.social.web.OAuth2RequestParameters.PARAMETER_STATE;

public abstract class AbstractOAuth2SignInController<A> {

  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractOAuth2SignInController.class);

  private final ConnectionFactoryLocator connectionFactoryLocator;
  private final UsersConnectionRepository usersConnectionRepository;
  private final SignInAdapter<A> signInAdapter;
  private final OAuth2SessionStrategy sessionStrategy;

  private String applicationUrl;
  private String defaultPostSignInUrl = "/";
  private OAuth2ConnectSupport OAuth2ConnectSupport;

  @Autowired
  public AbstractOAuth2SignInController(ConnectionFactoryLocator connectionFactoryLocator,
                                        UsersConnectionRepository usersConnectionRepository,
                                        SignInAdapter<A> signInAdapter,
                                        OAuth2SessionStrategy sessionStrategy) {
    this.connectionFactoryLocator = connectionFactoryLocator;
    this.usersConnectionRepository = usersConnectionRepository;
    this.signInAdapter = signInAdapter;
    this.sessionStrategy = sessionStrategy;
  }

  @PostConstruct
  public void initialize() {
    this.OAuth2ConnectSupport = new OAuth2ConnectSupport(sessionStrategy);
    this.OAuth2ConnectSupport.setUseAuthenticateUrl(true);
    if (this.applicationUrl != null) {
      this.OAuth2ConnectSupport.setApplicationUrl(applicationUrl);
    }
  }

  protected abstract Class<A> providerApiType();

  protected abstract URIBuilder signInRedirectURI();

  protected abstract URIBuilder signUpRedirectURI();

  @RequestMapping(method = RequestMethod.POST)
  public final RedirectView signIn(NativeWebRequest request) {
    try {
      ConnectionFactory<A> connectionFactory = connectionFactoryLocator.getConnectionFactory(providerApiType());
      MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
      return new RedirectView(OAuth2ConnectSupport.buildOAuthUrl(connectionFactory, request, parameters));
    } catch (Exception e) {
      LOGGER.error("Exception while building authorization URL: ", e);
      return redirect(signInRedirectURI().queryParam(PARAMETER_ERROR, ERROR_CODE_PROVIDER).build().toString());
    }
  }

  @RequestMapping(method = RequestMethod.GET, params = PARAMETER_CODE)
  public final RedirectView callback(@RequestParam(PARAMETER_CODE) String code,
                                     @RequestParam(PARAMETER_STATE) String state,
                                     NativeWebRequest request) {
    try {
      OAuth2ConnectionFactory<A> connectionFactory = (OAuth2ConnectionFactory<A>) connectionFactoryLocator.getConnectionFactory(providerApiType());
      Connection<A> connection = OAuth2ConnectSupport.completeConnection(connectionFactory, request);
      return handleSignIn(connection, request, state);
    } catch (Exception e) {
      LOGGER.error("Exception while completing OAuth2 connection: ", e);
      return redirect(signInRedirectURI().queryParam(PARAMETER_ERROR, ERROR_CODE_PROVIDER).build().toString());
    }
  }

  @RequestMapping(method = RequestMethod.GET, params = PARAMETER_ERROR)
  public final RedirectView errorCallback(@RequestParam(PARAMETER_ERROR) String error,
                                          @RequestParam(value = PARAMETER_ERROR_DESCRIPTION, required = false) String errorDescription,
                                          @RequestParam(value = PARAMETER_ERROR_URI, required = false) String errorUri) {
    LOGGER.error("Error during authorization: ", error);
    URIBuilder uriBuilder = signInRedirectURI().queryParam(PARAMETER_ERROR, error);
    if (errorDescription != null) {
      uriBuilder.queryParam(PARAMETER_ERROR_DESCRIPTION, errorDescription);
    }
    if (errorUri != null) {
      uriBuilder.queryParam(PARAMETER_ERROR_URI, errorUri);
    }
    return redirect(uriBuilder.build().toString());
  }

  @RequestMapping(method = RequestMethod.GET)
  public final RedirectView canceledAuthorizationCallback() {
    return redirect(signInRedirectURI().build().toString());
  }

  private RedirectView handleSignIn(Connection<A> connection,
                                    NativeWebRequest request,
                                    String state) {
    List<String> userIds = usersConnectionRepository.findUserIdsWithConnection(connection);
    if (userIds.size() == 0) {
      ProviderSignInAttempt signInAttempt = new ProviderSignInAttempt(connection.createData());
      sessionStrategy.setSignInAttempt(request, signInAttempt);
      return redirect(
          signUpRedirectURI()
              .queryParam(PARAMETER_STATE, state)
              .build()
              .toString()
      );
    } else if (userIds.size() == 1) {
      String userId = userIds.get(0);
      usersConnectionRepository.createConnectionRepository(userId).updateConnection(connection);
      return redirect(ofNullable(signInAdapter.signIn(userId, connection, request)).orElse(defaultPostSignInUrl));
    } else {
      return redirect(
          signInRedirectURI()
              .queryParam(PARAMETER_ERROR, ERROR_CODE_MULTIPLE_USERS)
              .build()
              .toString()
      );
    }
  }

  private RedirectView redirect(String url) {
    return new RedirectView(url, true);
  }
}

